// Good work here.  I did run into a couple of bugs:
// - When you click on an operation sign more than once
// you get the NaN error.
// - Also when you click on an operation and then on the '='
// that produces an NaN error as well.
// Otherwise your calcular worked well with the functionality
// you gave it.  Good work with the 30 second function.
// I was a bit disappointed with the lopsided team effort.
// That will be reflected in individual participation marks.
// You team had a total of 34 commits.

// Calculator - 8/10
// Microwave - 10/10
// Design    - 9/10
// Total     - 27/30

Memory = "0"; // initialise memory variable
Current = "0"; //   and value of Display ("current" value)
Operation = 0; // Records code for eg * / etc.
MAXLENGTH = 30; // maximum number of digits before decimal!

function addDigit(digit)
  { //ADD A DIGIT TO DISPLAY (kept as 'Current')
      if (Current.length > MAXLENGTH)
      {
          Current = "Aargh! Too long"; //limit length
      }
      else
      {
          if ((eval(Current) == 0) && (Current.indexOf(".") == -1))
          {
              Current = digit;
          }

          else
          {
              Current = Current + digit;
          }
      }

      document.getElementById('display').value = Current;
  }

function Dot()
  {
      if (Current.length == 0)
      {
          Current = "0.";
      }

      else
      {
          // Current.indexOf(".") returns 0 or 1 if there IS a '.' in Current
          // to compare for no '.', use -1
          if (Current.indexOf(".") == -1)
          {
              Current = Current + ".";
          }
      }

      document.getElementById('display').value = Current;

  }

function Clear()
  {
      Current = Memory;
      document.getElementById('display').value = Current;

  }

function AllClear()
  {
      Current = "0";
      Operation = 0;
      Memory = Current;
      document.getElementById('display').value = Current;

  }

function Operate(op)
  { //STORE OPERATION e.g. + * / etc.

      if (op.indexOf("*") > -1)
      {
          Operation = 1;
      } //codes for *

      if (op.indexOf("/") > -1)
      {
          Operation = 2;
      } // slash (divide)

      if (op.indexOf("+") > -1)
      {
          Operation = 3;
      } // sum

      if (op.indexOf("-") > -1)
      {
          Operation = 4;
      } // difference

      Memory = Current; //store value
      Current = ""; //or we could use "0"
      document.getElementById('display').value = Current;
  }

function Calculate()
  { //PERFORM CALCULATION (= button)
      if (Operation == 1)
      {   // multiplication because of what happened in operate()
          Current = eval(Memory) * eval(Current);
      }

      if (Operation == 2)
      {
        if (Current != 0){
            Current = eval(Memory) / eval(Current);
        }
        else
        {
          Current = "ERROR";
        }
      }

      if (Operation == 3)
      {
          Current = eval(Memory) + eval(Current);
      }

      if (Operation == 4)
      {
          Current = eval(Memory) - eval(Current);
      }

      Operation = 0; //clear operation
      Memory = "0"; //clear memory
      document.getElementById('display').value = Current;
  }
