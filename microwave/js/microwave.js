// Nice work with testing the bugs on this one.
// It runs quite well and its pretty robust.
// Good comments and well done.

var _time='';
var MAXLENGTH=4;
var counter;
var blinkCount=0;
var add30 = 0

function addDigit(addTime){
    //this function will allow to press the numbers...
  if(_time.length<MAXLENGTH){
    if(addTime != "30sec"){
      _time+=addTime;
    }else{
      var previousAdd30 = add30;
      add30 += 30;
      if (_time == '') {
        _time = '0';
         }
      if(counter != null){
       _time = counter.toString();
     }

     var timeStr = parseInt(_time) + parseInt(formatToMinutes(add30)) - parseInt(formatToMinutes(previousAdd30));
     _time = timeStr.toString();
   }

    counter=parseInt(_time);

    document.getElementById('mDisplay').value=formatTime(_time);
      }
  }


function formatToMinutes(secondsNumber){
  if (secondsNumber != 0) {
    var minutes = Math.floor(secondsNumber / 60);
    if (minutes == 0) {
      minutes = "";
    }
    var seconds = secondsNumber - (minutes * 60);
    minStr = minutes.toString();
    secStr = seconds.toString();
   if (secStr == '0') {
    secStr = '00';
  }
  return minStr + secStr;
}else{
  return "0";
}
}

function formatTime(pTime){
  //this function allows us to add numbers and inserts a ':' between 1st two and last 2 digits
  var temp="0000"+pTime;
  temp=temp.slice(-4);
  temp=temp.slice(0,2)+":"+temp.slice(-2);
  return temp;
}

function clearTime(){
  _time="";
  counter=0;
  add30 = 0;
  document.getElementById('mDisplay').value=formatTime(_time);
}

function startTimer(){
    if(counter<0){
      clearTimeout(mTimer);

      document.getElementById('start').disabled=false;
      return;
    }
    document.getElementById('mDisplay').value=formatTime(counter.toString());
    if(counter%100==0){
      counter=counter-40;
    }
    counter--;
    document.getElementById('mDisplay').style.backgroundColor = 'yellow';
    document.getElementById('start').disabled=true;
    document.getElementById('clear').disabled=true;
    document.getElementById('30s').disabled=true;
    // finally, we set our timer using the 'setTimeout()' function, and we
    // 'recall' this function again in 1000 milliseconds
    mTimer = setTimeout("startTimer()",1000);

    if(counter==0){
      blinkTimer = setInterval("blink()",1000);
      if (counter==0) {
        var snd = new Audio('sounds/Microwave_Beeping.mp3');
        snd.play();
    }
    }
}

function cancelTimer(){
    //this function will stop the countdown if pressed once, and if pressed the
    //second time will reset the timer(reset the display to 00:00)
    if(mTimer==null){
      _time="";
      counter=0;

      document.getElementById('mDisplay').value=formatTime(_time);
      document.getElementById('start').disabled=true;
      document.getElementById('mDisplay').style.backgroundColor = 'white';
    }
    add30 = 0;
    clearTimeout(mTimer);
    mTimer=null;
    document.getElementById('start').disabled=false;
    document.getElementById('clear').disabled=false;
    document.getElementById('30s').disabled=false;
}

function blink(){
  document.getElementById('mDisplay').value = "Done!";
  if(blinkCount==3){
    //clear interval
    clearInterval(blinkTimer);
    blinkCount=0;
    document.getElementById('mDisplay').style.backgroundColor = 'white';
    return;
  }
    if(document.getElementById('mDisplay').style.backgroundColor == 'yellow'){
      document.getElementById('mDisplay').style.backgroundColor = 'red';
  }else{
    document.getElementById('mDisplay').style.backgroundColor = 'yellow';
  }
  blinkCount++;
}
